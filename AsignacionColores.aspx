﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="AsignacionColores.aspx.cs" Inherits="AsignacionColores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <h2>Asignacion Colores</h2>
    <div>
        <asp:Label ID="_LabelError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
    </div>
    <div class="well col-md-12">
        <asp:Button ID="m_ButtonGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" OnClick="m_ButtonGuardar_Click" />
        <asp:GridView ID="m_GridViewColores" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-hover" DataSourceID="SqlDataSourceColores" OnRowDataBound="m_GridViewColores_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="COD. USUARIO" SortExpression="codusuario">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("codusuario") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="m_LabelCodUsuario" runat="server" Text='<%# Bind("codusuario") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="COLOR" SortExpression="color">
                    <ItemTemplate>
                        <asp:TextBox ID="m_TextBoxColor" runat="server" Text='<%# Bind("color") %>' CssClass="form-control" OnTextChanged="m_TextBoxColor_TextChanged1"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="descripcion" HeaderText="COMPRADOR(A)" SortExpression="descripcion" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSourceColores" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="cap_man_ColoresCompras_Retrieve" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

    </div>
</asp:Content>

