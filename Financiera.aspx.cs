﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Financiera : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            m_labelFecha.Text = System.DateTime.Now.Subtract(TimeSpan.FromDays(90)).ToShortDateString();
            _GridViewSinRotacionPreboPerece.DataBind();
            _GridViewSinRotacionPreboNoPerece.DataBind();
            _GridViewSinRotacionManonPerece.DataBind();
            _GridViewSinRotacionManonNoPerece.DataBind();
            _UpdatePanelPrebo.Update();
            _UpdatePanelManon.Update();
        }
    }
}