﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TEST.aspx.cs" Inherits="TEST" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dashboard ODC</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.10.2.min.js"></script>

    <script src="Scripts/bxslider/jquery.bxslider.min.js"></script>
    <link href="Scripts/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <style>
        .Fullscreen {
            width: 1890px !important;
        }

        .panel-wrapper {
            /*padding:0;*/
            padding-top: 5px;
            padding-left: 15px;
        }

        .HeaderFix {
            text-align: center;
            height: 45px;
            vertical-align: middle;
            margin: 0 0 5px 0 !important;
            padding: 3px 0 !important;
            font-weight: bold;
        }
    </style>

    <script>
        $(function () {
            $('.bxslider').bxSlider({
                auto: true,
                speed: 3000,
                slideMargin: 10,
                controls: true,
                pause: 10000,
                autoHover: true,
            });
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
        <ul class="bxslider">
            <li>
                <div class="Fullscreen">
                    <h1 style="background-color: #0EA554" class="HeaderFix"><b>SUCURSAL PREBO III</b><asp:Label ID="m_labelFecha" runat="server" Visible="False"></asp:Label>
                    </h1>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:UpdatePanel runat="server" ID="_updateTimePrebo" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <asp:UpdatePanel runat="server" ID="_UpdatePanelPrebo" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Timer runat="server" ID="_TimerPrebo" Interval="180000" OnTick="Timer1_Tick"></asp:Timer>
                                    <asp:GridView ID="_GridViewDelDiaPrebo" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceODCPorRecibir" CssClass="table table-striped table-hover" EmptyDataText="No hay ordenes de compra para el dia de Hoy">
                                        <Columns>
                                            <asp:TemplateField SortExpression="codusuario">
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelCodUsuario" runat="server" Text='<%# Bind("codusuario") %>' Visible="False"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="c_DOCUMENTO" HeaderText="# ORDEN" SortExpression="c_DOCUMENTO">
                                                <ItemStyle Font-Bold="True" Font-Size="Large" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="PROVEEDOR" SortExpression="c_DESCRIPCION">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("c_DESCRIPCION") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Bold="True" Width="620px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NO DESPACHADO">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="EN ESPERA">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RECIBIENDOSE">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RECIBIDO">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="enc_DespachoOdc_status" SortExpression="enc_DespachoOdc_status" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelStatus" runat="server" Text='<%# Bind("enc_DespachoOdc_status") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="enc_DespachoOdc_FueraDeFecha" SortExpression="enc_DespachoOdc_FueraDeFecha" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelFueraDeFecha" runat="server" Text='<%# Bind("enc_DespachoOdc_FueraDeFecha") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSourceODCPorRecibir" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rp_ListadoDeOdcPorRecibir" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSourceODCPorRecibir_Selecting">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="m_LabelFecha" DbType="Date" DefaultValue="" Name="FechaRecepcion" PropertyName="Text" />
                                            <asp:Parameter DefaultValue="UNI01" Name="sucursal" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="Fullscreen">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 style="background-color: #D9534F" class="HeaderFix">ODC POR VIAJAR</h1>
                            <asp:UpdatePanel runat="server" ID="_UpdatePanelPorViajar" UpdateMode="Conditional" ViewStateMode="Disabled" EnableViewState="False">
                                <ContentTemplate>
                                    <asp:Timer ID="_TimerPorViajar" runat="server" Interval="5000" OnTick="_TimerPorViajar_Tick"></asp:Timer>
                                    <asp:Label ID="m_LabelSemaforo" runat="server"></asp:Label>
                                    <div id="time"> </div>
                                    <asp:GridView ID="m_GridViewODCSinViajar" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-hover" EmptyDataText="Todas las Ordenes viajaron de manera Exitosa." DataSourceID="SqlDataSourceTest">
                                        <Columns>
                                            <asp:BoundField DataField="c_DOCUMENTO" HeaderText="# ORDEN" SortExpression="c_DOCUMENTO">
                                                <ItemStyle Font-Bold="True" Font-Size="Medium" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="c_DESCRIPCION" HeaderText="PROVEEDOR" SortExpression="c_DESCRIPCION">
                                                <ItemStyle Font-Bold="True" Font-Size="Medium" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="c_status" HeaderText="c_status" SortExpression="c_status" Visible="False" />
                                            <asp:BoundField DataField="C_DESPACHAR" HeaderText="C_DESPACHAR" SortExpression="C_DESPACHAR" Visible="False" />
                                            <asp:BoundField DataField="d_fecha_recepcion" HeaderText="d_fecha_recepcion" SortExpression="d_fecha_recepcion" Visible="False" />
                                            <asp:BoundField DataField="DU_FECHAVENCIMIENTO" HeaderText="DU_FECHAVENCIMIENTO" SortExpression="DU_FECHAVENCIMIENTO" Visible="False" />
                                            <asp:BoundField DataField="d_FECHA" HeaderText="FECHA" ReadOnly="True" SortExpression="d_FECHA" />
                                            <asp:BoundField DataField="c_CODLOCALIDAD" HeaderText="c_CODLOCALIDAD" SortExpression="c_CODLOCALIDAD" Visible="False" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSourceTest" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rps_ODCSinViajar" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                    <asp:SqlDataSource ID="SqlDataSourceODCSinViajar" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rps_ODCSinViajar" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSourceODCSinViajar_Selecting" CancelSelectOnNullParameter="False"></asp:SqlDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="col-md-6">
                            <h1 style="background-color: #D9534F" class="HeaderFix">ODC POR VENCER</h1>
                            <asp:GridView ID="_GridViewPorVencer" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-hover" DataSourceID="SqlDataSourcePorVencer">
                                <Columns>
                                    <asp:TemplateField ConvertEmptyStringToNull="False" SortExpression="codusuario">
                                        <ItemTemplate>
                                            <asp:Label ID="m_LabelCodUsuario" runat="server" Text='<%# Eval("codusuario") %>' Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="c_DOCUMENTO" HeaderText="# Orden" SortExpression="c_DOCUMENTO">
                                        <ItemStyle Font-Bold="True" Font-Size="Large" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="c_DESCRIPCION" HeaderText="PROVEEDOR" SortExpression="c_DESCRIPCION">
                                        <ItemStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DU_FECHAVENCIMIENTO" HeaderText="VENCIMIENTO" SortExpression="DU_FECHAVENCIMIENTO" DataFormatString="{0:d}" />
                                    <asp:BoundField DataField="d_FECHA" HeaderText="d_FECHA" SortExpression="d_FECHA" Visible="False" />
                                    <asp:BoundField DataField="C_DESPACHAR" HeaderText="C_DESPACHAR" SortExpression="C_DESPACHAR" Visible="False" />
                                    <asp:BoundField DataField="c_CODLOCALIDAD" HeaderText="c_CODLOCALIDAD" SortExpression="c_CODLOCALIDAD" Visible="False" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSourcePorVencer" runat="server" CancelSelectOnNullParameter="False" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rp_OdcPorVencer" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSourcePorVencer_Selecting">
                                <SelectParameters>
                                    <asp:Parameter Name="Sucursal" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <h1></h1>
            </li>
        </ul>
    </form>
</body>
</html>
