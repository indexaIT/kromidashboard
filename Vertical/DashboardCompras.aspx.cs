﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Vertical_DashboardCompras : System.Web.UI.Page
{
    private string Cadena = ConfigurationManager.ConnectionStrings["DashboardsConnectionString"].ConnectionString.ToString();
    private string Cadena2 = ConfigurationManager.ConnectionStrings["DeveloperProceduresConnectionString"].ConnectionString.ToString();
    private int PreCols = 1;

    SqlConnection Connection;
    SqlCommand Select = new SqlCommand();


    protected void Page_Load(object sender, EventArgs e)
    {
        PreCols = 1;
        if (!IsPostBack)
        {

            //string script = "";
            //script += "$(function () {";
            //script += "$('.bxslider').bxSlider({";
            //script += "auto: true,";
            //script += "speed: 3000,";
            //script += "slideMargin: 5,";
            //script += "controls: true,";
            //script += "pause: 10000,";
            //script += "autoHover: true";
            //script += "});";
            //script += "});";
            ////ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "AsyncScript", script, True)
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Slider", script, true);
            //string script="";
            //script += "$(function () {";
            //script += "$('#slider-id').liquidSlider({";
            //script += "autoSlide: true,";
            //script += "autoSlideInterval: 10000,";
            //script += "pauseOnHover: false,";
            //script += "forceAutoSlide: true,";
            //script += "});";
            //script += "});";
            ////ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "AsyncScript", script, True)
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Slider", script, true);
            try
            {
                m_labelFecha.Text = System.DateTime.Now.ToShortDateString();
                //_GridViewDelDiaPrebo.DataBind();
                //_GridViewDelDiaManon.DataBind();
                _GridViewPorVencer.DataBind();
                m_GridViewODCSinViajar.DataBind();
            }
            catch (Exception ex)
            {
            }
        }
        else
        {
            //if(Request.Params["__EVENTTARGET"].Contains("_TimerPorViajar"))
            //{
            //    m_GridViewODCSinViajar.DataSource = PorViajar();
            //    m_GridViewODCSinViajar.DataBind();
            //}
        }
    }

    protected DataTable PorViajar()
    {
        DataTable dt = new DataTable();
        try
        {
            SqlConnection Connection = new SqlConnection(Cadena2);
            Select.CommandType = CommandType.StoredProcedure;
            Select.Connection = Connection;
            Select.CommandText = "rps_ODCSinViajar";
            Select.Parameters.Clear();
            Connection.Open();
            SqlDataAdapter da = new SqlDataAdapter(Select);
            da.Fill(dt);
            Connection.Close();
            return dt;
        }
        catch (Exception ex)
        {
            return dt;
        }
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        //m_LabelStatus.Text = "Ultima Actualizacion: " + DateTime.Now.ToLongTimeString();
        try
        {
            Prebo.DataBind();
            _UpdatePanelPrebo.Update();
            //_updateTimePrebo.Update();
            //_GridViewDelDiaPrebo.DataBind();
            //_UpdatePanelPrebo.Update();
            //_GridViewDelDiaManon.DataBind();
            //_UpdatePanelManon.Update();
            //SqlDataSourceTest.DataBind();
            //m_GridViewODCSinViajar.DataBind();            
            //_UpdatePanelPorViajar.Update();
        }
        catch (Exception ex)
        {

        }
        //_GridViewODCSinViajarPrebo.DataBind();
        //_GridViewODCSinViajarManon.DataBind();
        //string script = "api2.setNextPanel('right');api2.updateClass($(this))";
        //Page.ClientScript.RegisterStartupScript(this.GetType(),
        //    "Mast", script);
        //api2.setNextPanel('right');api2.updateClass($(this))
    }
    protected void color(object sender, GridViewRowEventArgs e, int status, bool status2, string fecha)
    {
        if (status2)
        {
            //COLOR AL PROVEEDOR FUERA DE FECHA
            if (fecha != "")
            {
                if (Convert.ToDateTime(fecha).Date < System.DateTime.Now.Date)
                {
                    e.Row.Cells[PreCols + 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#611BBD");
                    e.Row.Cells[PreCols + 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#130269");
                    e.Row.Cells[PreCols + 1].ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    e.Row.Cells[PreCols + 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#AB0606");
                    e.Row.Cells[PreCols + 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#AB0606");
                    e.Row.Cells[PreCols + 1].ForeColor = System.Drawing.Color.White;
                }
            }    
        }

        switch (status)
        {
            case (0):
                e.Row.Cells[PreCols + 2].BackColor = System.Drawing.ColorTranslator.FromHtml("#D9534F");
                e.Row.Cells[PreCols + 2].BorderColor = System.Drawing.ColorTranslator.FromHtml("#AC2925");
                break;
            case (1):
                e.Row.Cells[PreCols + 3].BackColor = System.Drawing.ColorTranslator.FromHtml("#EC971F");
                e.Row.Cells[PreCols + 3].BorderColor = System.Drawing.ColorTranslator.FromHtml("#D58512");
                break;
            case (2):
                e.Row.Cells[PreCols + 4].BackColor = System.Drawing.ColorTranslator.FromHtml("#3071A9");
                e.Row.Cells[PreCols + 4].BorderColor = System.Drawing.ColorTranslator.FromHtml("#285E8E");
                break;
            case (3):
                e.Row.Cells[PreCols + 5].BackColor = System.Drawing.ColorTranslator.FromHtml("#449D44");
                e.Row.Cells[PreCols + 5].BorderColor = System.Drawing.ColorTranslator.FromHtml("#4CAE4C");
                break;
        }

        //ap_EfectividadOdc
        //@Odc VARCHAR (50)
        //try
        //{
        //    //m_LabelFecha

        //    string grid = ((GridView)e.Row.NamingContainer).ID.ToString();
        //    SqlConnection Connection = new SqlConnection(Cadena2);
        //    Select.CommandType = CommandType.StoredProcedure;
        //    Select.Connection = Connection;
        //    Select.CommandText = "rp_ListadoDeOdcPorRecibirCentral";
        //    Select.Parameters.Clear();
        //    Select.Parameters.Add("@FechaRecepcion", SqlDbType.Date).Value = Convert.ToDateTime(m_labelFecha.Text).Date;
        //    switch (grid)
        //    {
        //        case "Prebo":
        //            Select.Parameters.Add("@sucursal", SqlDbType.VarChar).Value = "UNI01";
        //            break;
        //        case "Manon":
        //            Select.Parameters.Add("@sucursal", SqlDbType.VarChar).Value = "UNI02";
        //            break;
        //    }

        //    Connection.Open();
        //    SqlDataReader data = Select.ExecuteReader();
        //    var dt = new DataTable();
        //    dt.Load(data);
        //    Connection.Close();
        //    if (dt.Rows.Count > 0)
        //    {
        //        e.Row.Cells[5].Text = dt.Rows[0]["enc_DespachoOdc_Efectividad"].ToString();
        //    }
        //}
        //catch (Exception ex)
        //{

        //}
    }

    protected void _GridViewDelDiaPrebo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                int status = Convert.ToInt32((e.Row.FindControl("m_LabelStatus") as Label).Text);
                bool status2 = Convert.ToBoolean((e.Row.FindControl("m_LabelFueraDeFecha") as Label).Text);
                string fecha = (e.Row.FindControl("m_LabelFecha") as Label).Text;
                string codusuario = (e.Row.FindControl("m_LabelCodUsuario") as Label).Text;
                if (codusuario != "")
                {
                    SqlConnection Connection = new SqlConnection(Cadena2);
                    Select.CommandType = CommandType.StoredProcedure;
                    Select.Connection = Connection;
                    Select.CommandText = "ap_man_ColoresCompras_Retrieve";
                    Select.Parameters.Clear();
                    Select.Parameters.Add("@codusuario", SqlDbType.VarChar).Value = codusuario;
                    Connection.Open();
                    SqlDataReader data = Select.ExecuteReader();
                    var dt = new DataTable();
                    dt.Load(data);
                    Connection.Close();
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["color"].ToString() != "")
                        {
                            e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(dt.Rows[0]["color"].ToString());
                        }
                    }
                }
                color(sender, e, status, status2,fecha);


            }
            catch (Exception ex)
            {

            }
        }
    }
    protected void _GridViewPorVencer_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                string codusuario = (e.Row.FindControl("m_LabelCodUsuario") as Label).Text;
                if (codusuario != "")
                {
                    SqlConnection Connection = new SqlConnection(Cadena2);
                    Select.CommandType = CommandType.StoredProcedure;
                    Select.Connection = Connection;
                    Select.CommandText = "ap_man_ColoresCompras_Retrieve";
                    Select.Parameters.Clear();
                    Select.Parameters.Add("@codusuario", SqlDbType.VarChar).Value = codusuario;
                    Connection.Open();
                    SqlDataReader data = Select.ExecuteReader();
                    var dt = new DataTable();
                    dt.Load(data);
                    Connection.Close();
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["color"].ToString() != "")
                        {
                            e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(dt.Rows[0]["color"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
    protected void _GridViewODCSinViajarPrebo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                string codusuario = (e.Row.FindControl("m_LabelCodUsuario") as Label).Text;
                if (codusuario != "")
                {
                    SqlConnection Connection = new SqlConnection(Cadena2);
                    Select.CommandType = CommandType.StoredProcedure;
                    Select.Connection = Connection;
                    Select.CommandText = "ap_man_ColoresCompras_Retrieve";
                    Select.Parameters.Clear();
                    Select.Parameters.Add("@codusuario", SqlDbType.VarChar).Value = codusuario;
                    Connection.Open();
                    SqlDataReader data = Select.ExecuteReader();
                    var dt = new DataTable();
                    dt.Load(data);
                    Connection.Close();
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["color"].ToString() != "")
                        {
                            e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml(dt.Rows[0]["color"].ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
    protected void _TimerPorViajar_Tick(object sender, EventArgs e)
    {
        try
        {
            m_GridViewODCSinViajar.DataBind();
            m_LabelSemaforo.Text = System.DateTime.Now.ToString();
            _UpdatePanelPorViajar.Update();
        }
        catch (Exception ex)
        {

        }
    }
    protected void SqlDataSourceODCSinViajar_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.CommandTimeout = 0;
    }
    protected void SqlDataSourcePorVencer_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.CommandTimeout = 0;
    }
    protected void SqlDataSourceODCPorRecibir_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.CommandTimeout = 0;
    }
    protected void SqlDataSourceODCPorRecibirManon_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {
        e.Command.CommandTimeout = 0;
    }
    protected void _TimerManon_Tick(object sender, EventArgs e)
    {
        //_GridViewDelDiaManon.DataBind();
        //_UpdatePanelManon.Update();
    }
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Label color = e.Item.FindControl("colorLabel") as Label;
        (e.Item.FindControl("m_TextBoxColor") as TextBox).BackColor = System.Drawing.ColorTranslator.FromHtml(color.Text);
    }
}