﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DashboardCompras.aspx.cs" Inherits="DashboardCompras" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dashboard ODC</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />


    <script src="Scripts/jquery-1.10.2.min.js"></script>

    <script src="Scripts/bxslider/jquery.bxslider.min.js"></script>
    <link href="Scripts/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <style>
        .Fullscreen {
            width: 1900px !important;
            padding-left:5px;
        }

        .panel-wrapper {
            padding-top: 5px;
            padding-left: 15px;
        }

        .HeaderFix {
            text-align: center;
            height: 45px;
            vertical-align: middle;
            margin: 0 0 5px 0 !important;
            padding: 3px 0 !important;
            font-weight: bold;
            border-radius: 15px;
        }

        .bx-prev {
            position: absolute !important;
            top: 50px !important;
        }

        .bx-next {
            position: absolute !important;
            top: 50px !important;
        }

        .bx-controls-auto {
            position: absolute !important;
            top: 10px !important;
            text-align: left !important;
            padding-left: 5px;
            width:10% !important;
            height: 10px;
        }
    </style>
    <script>

        $(function () {
            $('.bxslider').bxSlider({
                auto: true,
                speed: 3000,
                slideMargin: 5,
                controls: true,
                pause: 30000,
                //autoHover: true,
                autoControls: true,
                pager: false
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <ul class="bxslider">
            <li>
                <div class="Fullscreen">
                    <h1 style="background-color: #0EA554" class="HeaderFix"><b>SUCURSAL PREBO III</b><asp:Label ID="m_labelFecha" runat="server" Visible="False"></asp:Label>
                    </h1>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:UpdatePanel runat="server" ID="_updateTimePrebo" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-8">
                            <asp:UpdatePanel runat="server" ID="_UpdatePanelPrebo" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Timer runat="server" ID="_TimerPrebo" OnTick="Timer1_Tick"></asp:Timer>
                                    <asp:GridView ID="_GridViewDelDiaPrebo" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceODCPorRecibir" CssClass="table table-striped table-hover" OnRowDataBound="_GridViewDelDiaPrebo_RowDataBound" EmptyDataText="No hay ordenes de compra para el dia de Hoy">
                                        <Columns>
                                            <asp:TemplateField SortExpression="codusuario">
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelCodUsuario" runat="server" Text='<%# Bind("codusuario") %>' Visible="False"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="c_DOCUMENTO" HeaderText="# ORDEN" SortExpression="c_DOCUMENTO">
                                                <ItemStyle Font-Bold="True" Font-Size="Large" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="PROVEEDOR" SortExpression="c_DESCRIPCION">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("c_DESCRIPCION") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Font-Bold="True" Width="620px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="NO DESPACHADO">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="EN ESPERA">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RECIBIENDOSE">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RECIBIDO">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text='<%# Eval("enc_DespachoOdc_Efectividad") + "%" %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="enc_DespachoOdc_status" SortExpression="enc_DespachoOdc_status" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelStatus" runat="server" Text='<%# Bind("enc_DespachoOdc_status") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="enc_DespachoOdc_FueraDeFecha" SortExpression="enc_DespachoOdc_FueraDeFecha" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelFueraDeFecha" runat="server" Text='<%# Bind("enc_DespachoOdc_FueraDeFecha") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="enc_DespachoOdc_FechaOriginal" SortExpression="enc_DespachoOdc_FechaOriginal" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelFecha" runat="server" Text='<%# Bind("enc_DespachoOdc_FechaOriginal") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- enc_DespachoOdc_FechaOriginal --%>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSourceODCPorRecibir" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rp_ListadoDeOdcPorRecibirCentral" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSourceODCPorRecibir_Selecting" CancelSelectOnNullParameter="False">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="m_LabelFecha" DbType="Date" DefaultValue="" Name="FechaRecepcion" PropertyName="Text" />
                                            <asp:Parameter DefaultValue="UNI01" Name="sucursal" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="Fullscreen">
                    <h1 style="background-color: #F87F30" class="HeaderFix"><b>SUCURSAL MAÑONGO</b></h1>
                    <div class="row">
                        <div class="col-lg-8">
                            <asp:UpdatePanel runat="server" ID="_UpdatePanelManon" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Timer runat="server" ID="_TimerManon" Interval="40000" OnTick="_TimerManon_Tick"></asp:Timer>
                                    <asp:GridView ID="_GridViewDelDiaManon" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceODCPorRecibirManon" CssClass="table table-striped table-hover" OnRowDataBound="_GridViewDelDiaPrebo_RowDataBound" EmptyDataText="No hay ordenes de compra para el dia de Hoy">
                                        <Columns>
                                            <asp:TemplateField SortExpression="codusuario">
                                                <EditItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("codusuario") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelCodUsuario" runat="server" Text='<%# Bind("codusuario") %>' Visible="False"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="c_DOCUMENTO" HeaderText="# ORDEN" SortExpression="c_DOCUMENTO">
                                                <ItemStyle Font-Bold="True" Font-Size="Large" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="c_DESCRIPCION" HeaderText="PROVEEDOR" SortExpression="c_DESCRIPCION">
                                                <ItemStyle Width="620px" Font-Bold="True" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="NO DESPACHADO">
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="EN ESPERA">
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RECIBIENDOSE">
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RECIBIDO">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text='<%# Eval("enc_DespachoOdc_Efectividad") + "%" %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="enc_DespachoOdc_status" SortExpression="enc_DespachoOdc_status" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelStatus" runat="server" Text='<%# Bind("enc_DespachoOdc_status") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="enc_DespachoOdc_FueraDeFecha" SortExpression="enc_DespachoOdc_FueraDeFecha" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelFueraDeFecha" runat="server" Text='<%# Bind("enc_DespachoOdc_FueraDeFecha") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="enc_DespachoOdc_FechaOriginal" SortExpression="enc_DespachoOdc_FechaOriginal" Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="m_LabelFecha" runat="server" Text='<%# Bind("enc_DespachoOdc_FechaOriginal") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSourceODCPorRecibirManon" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rp_ListadoDeOdcPorRecibirCentral" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSourceODCPorRecibirManon_Selecting" CancelSelectOnNullParameter="False">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="m_LabelFecha" DbType="Date" DefaultValue="" Name="FechaRecepcion" PropertyName="Text" />
                                            <asp:Parameter DefaultValue="UNI02" Name="sucursal" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>
                </div>
            </li>
                <%--           <div class="Fullscreen">
                <h1 style="background-color: #1E3B97;color:white" class="HeaderFix"><b>SUCURSAL SAN FELIPE</b></h1>
                <div class="row">
                    <div class="col-lg-8">
                        <asp:UpdatePanel runat="server" ID="_UpdatePanelSanFelipe" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Timer runat="server" ID="Timer2" Interval="10000" OnTick="Timer1_Tick"></asp:Timer>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>
           <div class="Fullscreen">
                <h1 style="background-color: #DF0889" class="HeaderFix"><b>SUCURSAL SAN DIEGO</b></h1>
                <div class="row">
                    <div class="col-lg-8">
                        <asp:UpdatePanel runat="server" ID="_UpdatePanelSanDiego" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Timer runat="server" ID="Timer3" Interval="10000" OnTick="Timer1_Tick"></asp:Timer>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>--%>
            <li>
                <div class="Fullscreen">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 style="background-color: #D9534F" class="HeaderFix">ODC POR VIAJAR</h1>
                            <asp:UpdatePanel runat="server" ID="_UpdatePanelPorViajar" UpdateMode="Conditional" ViewStateMode="Disabled">
                                <ContentTemplate>
                                    <asp:Timer ID="_TimerPorViajar" runat="server" Interval="120000" OnTick="_TimerPorViajar_Tick"></asp:Timer>
                                    <div>
                                        <asp:Label ID="m_LabelSemaforo" runat="server" Text=""></asp:Label>
                                    </div>
                                    <asp:GridView ID="m_GridViewODCSinViajar" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-hover" EmptyDataText="Todas las Ordenes viajaron de manera Exitosa." DataSourceID="SqlDataSourceTest">
                                        <Columns>
                                            <asp:BoundField DataField="c_DOCUMENTO" HeaderText="# ORDEN" SortExpression="c_DOCUMENTO">
                                                <ItemStyle Font-Bold="True" Font-Size="Medium" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="c_DESCRIPCION" HeaderText="PROVEEDOR" SortExpression="c_DESCRIPCION">
                                                <ItemStyle Font-Bold="True" Font-Size="Medium" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="c_status" HeaderText="c_status" SortExpression="c_status" Visible="False" />
                                            <asp:BoundField DataField="C_DESPACHAR" HeaderText="C_DESPACHAR" SortExpression="C_DESPACHAR" Visible="False" />
                                            <asp:BoundField DataField="d_fecha_recepcion" HeaderText="d_fecha_recepcion" SortExpression="d_fecha_recepcion" Visible="False" />
                                            <asp:BoundField DataField="DU_FECHAVENCIMIENTO" HeaderText="DU_FECHAVENCIMIENTO" SortExpression="DU_FECHAVENCIMIENTO" Visible="False" />
                                            <asp:BoundField DataField="d_FECHA" HeaderText="FECHA" ReadOnly="True" SortExpression="d_FECHA" />
                                            <asp:BoundField DataField="c_CODLOCALIDAD" HeaderText="c_CODLOCALIDAD" SortExpression="c_CODLOCALIDAD" Visible="False" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSourceTest" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rps_ODCSinViajar" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                                    <asp:SqlDataSource ID="SqlDataSourceODCSinViajar" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rps_ODCSinViajar" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSourceODCSinViajar_Selecting" CancelSelectOnNullParameter="False"></asp:SqlDataSource>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="col-md-6">
                            <h1 style="background-color: #D9534F" class="HeaderFix">ODC POR VENCER</h1>
                            <asp:GridView ID="_GridViewPorVencer" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-hover" DataSourceID="SqlDataSourcePorVencer" OnRowDataBound="_GridViewPorVencer_RowDataBound">
                                <Columns>
                                    <asp:TemplateField ConvertEmptyStringToNull="False" SortExpression="codusuario">
                                        <ItemTemplate>
                                            <asp:Label ID="m_LabelCodUsuario" runat="server" Text='<%# Eval("codusuario") %>' Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="c_DOCUMENTO" HeaderText="# Orden" SortExpression="c_DOCUMENTO">
                                        <ItemStyle Font-Bold="True" Font-Size="Large" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="c_DESCRIPCION" HeaderText="PROVEEDOR" SortExpression="c_DESCRIPCION">
                                        <ItemStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DU_FECHAVENCIMIENTO" HeaderText="VENCIMIENTO" SortExpression="DU_FECHAVENCIMIENTO" DataFormatString="{0:d}" />
                                    <asp:BoundField DataField="d_FECHA" HeaderText="d_FECHA" SortExpression="d_FECHA" Visible="False" />
                                    <asp:BoundField DataField="C_DESPACHAR" HeaderText="C_DESPACHAR" SortExpression="C_DESPACHAR" Visible="False" />
                                    <asp:BoundField DataField="c_CODLOCALIDAD" HeaderText="c_CODLOCALIDAD" SortExpression="c_CODLOCALIDAD" Visible="False" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSourcePorVencer" runat="server" CancelSelectOnNullParameter="False" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rp_OdcPorVencer" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSourcePorVencer_Selecting">
                                <SelectParameters>
                                    <asp:Parameter Name="Sucursal" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="Fullscreen">
                    <h1 class="HeaderFix">LEYENDA</h1>
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="HeaderFix" style="background-color: #0EA554;">SUCURSAL PREBO</h2>
                        </div>
                        <div class="col-md-6">
                            <h2 class="HeaderFix" style="background-color: #F87F30;">SUCURSAL MAÑONGO</h2>
                        </div>
                        <div class="col-md-6">
                            <h2 class="HeaderFix" style="background-color: #1E3B97;color:white;">SUCURSAL SAN FELIPE</h2>
                        </div>
                        <div class="col-md-6">
                            <h2 class="HeaderFix" style="background-color: #DF0889;">SUCURSAL SAN DIEGO</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="HeaderFix">COMPRADORES(AS)</h2>
                        </div>
                        <div class="col-md-10 col-md-offset-1">
                            <asp:DataList ID="DataList1" runat="server" CssClass="table table-striped table-hover" DataSourceID="SqlDataSourceLeyenda" GridLines="Both" OnItemDataBound="DataList1_ItemDataBound" RepeatColumns="5" RepeatDirection="Horizontal">
                                <ItemStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" Wrap="False" />
                                <ItemTemplate>
                                        <asp:Label ID="colorLabel" runat="server" Text='<%# Eval("color") %>' Visible="False" />
                                        <br />
                                        Nombre:
                                        <asp:Label ID="descripcionLabel" runat="server" Font-Bold="True" Font-Size="Large" Text='<%# Eval("descripcion") %>' />
                                        <br />
                                        <asp:TextBox ID="m_TextBoxColor" runat="server" CssClass="form-control" Enabled="False" Width="50px"></asp:TextBox>
                                        <br />
                                </ItemTemplate>
                            </asp:DataList>
                            <asp:SqlDataSource ID="SqlDataSourceLeyenda" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="cap_man_ColoresCompras_Retrieve" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DashboardsConnectionString %>" SelectCommand="SELECT [man_Sedes_Id], [man_Sedes_Nombre] FROM [man_Sedes]"></asp:SqlDataSource>
    </form>
</body>
</html>
