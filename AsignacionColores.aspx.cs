﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AsignacionColores : System.Web.UI.Page
{
    private string Cadena = ConfigurationManager.ConnectionStrings["DeveloperProceduresConnectionString"].ConnectionString.ToString();
    SqlConnection Connection;
    SqlCommand Select = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void m_ButtonGuardar_Click(object sender, EventArgs e)
    {
        SqlConnection Connection = new SqlConnection(Cadena);
        Select.CommandType = CommandType.StoredProcedure;
        Select.Connection = Connection;
        Select.CommandText = "ap_man_ColoresCompras_Update";
        foreach (GridViewRow linea in m_GridViewColores.Rows)
        {
            Select.Parameters.Clear();
            Select.Parameters.Add("@codusuario", SqlDbType.VarChar).Value = (linea.FindControl("m_LabelCodUsuario") as Label).Text;
            Select.Parameters.Add("@color", SqlDbType.VarChar).Value = (linea.FindControl("m_TextBoxColor") as TextBox).Text;
            Connection.Open();
            Select.ExecuteNonQuery();
            Connection.Close();
            m_GridViewColores.DataBind();
        }
    }
    protected void m_TextBoxColor_TextChanged(object sender, EventArgs e)
    {

    }
    protected void m_TextBoxColor_TextChanged1(object sender, EventArgs e)
    {
        try
        {
            (sender as TextBox).BackColor = System.Drawing.ColorTranslator.FromHtml((sender as TextBox).Text);
        }
        catch(Exception ex)
        { }
    }
    protected void m_GridViewColores_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //m_TextBoxColor
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                if((e.Row.FindControl("m_TextBoxColor") as TextBox).Text != "")
                {
                    (e.Row.FindControl("m_TextBoxColor") as TextBox).BackColor = System.Drawing.ColorTranslator.FromHtml((e.Row.FindControl("m_TextBoxColor") as TextBox).Text);
                }
            }
            catch(Exception ex)
            { }
        }
    }
}