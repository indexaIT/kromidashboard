﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Almacenes_Mañongo : System.Web.UI.Page
{
    private string Cadena = ConfigurationManager.ConnectionStrings["DashboardsConnectionString"].ConnectionString.ToString();
    private string Cadena2 = ConfigurationManager.ConnectionStrings["DeveloperProceduresConnectionString"].ConnectionString.ToString();
    private int PreCols = 1;

    SqlConnection Connection;
    SqlCommand Select = new SqlCommand();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                m_labelFecha.Text = System.DateTime.Now.ToShortDateString();
                _GridViewDelDiaManon.DataBind();
                _UpdatePanelManon.Update();
            }
            catch (Exception ex)
            {
            }
        }
    }
    protected void _GridViewDelDiaPrebo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                int status = Convert.ToInt32((e.Row.FindControl("m_LabelStatus") as Label).Text);
                bool status2 = Convert.ToBoolean((e.Row.FindControl("m_LabelFueraDeFecha") as Label).Text);
                color(sender, e, status, status2);
            }
            catch (Exception ex)
            {

            }
        }
    }
    protected void SqlDataSourceODCPorRecibirManon_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
    protected void _TimerManon_Tick(object sender, EventArgs e)
    {
        try
        {
            _GridViewDelDiaManon.DataBind();
            _UpdatePanelManon.Update();
        }
        catch (Exception ex)
        { }
    }
    protected void color(object sender, GridViewRowEventArgs e, int status, bool status2)
    {
        if (status2)
        {
            //COLOR AL PROVEEDOR FUERA DE FECHA
            e.Row.Cells[PreCols + 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#611BBD");
            e.Row.Cells[PreCols + 1].BackColor = System.Drawing.ColorTranslator.FromHtml("#130269");
            e.Row.Cells[PreCols + 1].ForeColor = System.Drawing.Color.White;
        }

        switch (status)
        {
            case (0):
                e.Row.Cells[PreCols + 2].BackColor = System.Drawing.ColorTranslator.FromHtml("#D9534F");
                e.Row.Cells[PreCols + 2].BorderColor = System.Drawing.ColorTranslator.FromHtml("#AC2925");
                break;
            case (1):
                e.Row.Cells[PreCols + 3].BackColor = System.Drawing.ColorTranslator.FromHtml("#EC971F");
                e.Row.Cells[PreCols + 3].BorderColor = System.Drawing.ColorTranslator.FromHtml("#D58512");
                break;
            case (2):
                e.Row.Cells[PreCols + 4].BackColor = System.Drawing.ColorTranslator.FromHtml("#3071A9");
                e.Row.Cells[PreCols + 4].BorderColor = System.Drawing.ColorTranslator.FromHtml("#285E8E");
                break;
            case (3):
                e.Row.Cells[PreCols + 5].BackColor = System.Drawing.ColorTranslator.FromHtml("#449D44");
                e.Row.Cells[PreCols + 5].BorderColor = System.Drawing.ColorTranslator.FromHtml("#4CAE4C");

                //ap_EfectividadOdc
                //@Odc VARCHAR (50)
                try
                {
                    SqlConnection Connection = new SqlConnection(Cadena);
                    Select.CommandType = CommandType.StoredProcedure;
                    Select.Connection = Connection;
                    Select.CommandText = "ap_EfectividadOdc";
                    Select.Parameters.Clear();
                    Select.Parameters.Add("@Odc", SqlDbType.VarChar).Value = e.Row.Cells[0].Text;
                    Connection.Open();
                    SqlDataReader data = Select.ExecuteReader();
                    var dt = new DataTable();
                    dt.Load(data);
                    Connection.Close();
                    if (dt.Rows.Count > 0)
                    {
                        e.Row.Cells[5].Text = dt.Rows[0]["ap_EfectividadOdc"].ToString();
                    }
                }
                catch (Exception ex)
                {

                }
                break;
        }
    }
}