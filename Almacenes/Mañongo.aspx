﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Mañongo.aspx.cs" Inherits="Almacenes_Mañongo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dashboard Almacen Mañongo</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />


    <script src="../Scripts/jquery-1.10.2.min.js"></script>

    <script src="../Scripts/bxslider/jquery.bxslider.min.js"></script>
    <link href="../Scripts/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <style>
        .Fullscreen {
            width: 1900px !important;
            padding-left: 5px;
        }

        .panel-wrapper {
            padding-top: 5px;
            padding-left: 15px;
        }

        .HeaderFix {
            text-align: center;
            height: 45px;
            vertical-align: middle;
            margin: 0 0 5px 0 !important;
            padding: 3px 0 !important;
            font-weight: bold;
            border-radius: 15px;
        }

        .bx-prev {
            position: absolute !important;
            top: 50px !important;
        }

        .bx-next {
            position: absolute !important;
            top: 50px !important;
        }

        .bx-controls-auto {
            position: absolute !important;
            top: 10px !important;
            text-align: left !important;
            padding-left: 5px;
            width: 10% !important;
            height: 10px;
        }
    </style>
    <script>

        $(function () {
            $('.bxslider').bxSlider({
                auto: true,
                speed: 3000,
                slideMargin: 5,
                controls: true,
                pause: 30000,
                //autoHover: true,
                autoControls: true,
                pager: false
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="Fullscreen">
            <h1 style="background-color: #F87F30" class="HeaderFix"><b>SUCURSAL MAÑONGO<asp:Label ID="m_labelFecha" runat="server" Visible="False"></asp:Label>
            </b></h1>
            <div class="row">
                <div class="col-lg-8">
                    <asp:UpdatePanel runat="server" ID="_UpdatePanelManon" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Timer runat="server" ID="_TimerManon" Interval="40000" OnTick="_TimerManon_Tick"></asp:Timer>
                            <asp:GridView ID="_GridViewDelDiaManon" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceODCPorRecibirManon" CssClass="table table-striped table-hover" OnRowDataBound="_GridViewDelDiaPrebo_RowDataBound" EmptyDataText="No hay ordenes de compra para el dia de Hoy">
                                <Columns>
                                    <asp:TemplateField SortExpression="codusuario">
                                        <EditItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("codusuario") %>'></asp:Label>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="m_LabelCodUsuario" runat="server" Text='<%# Bind("codusuario") %>' Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="c_DOCUMENTO" HeaderText="# ORDEN" SortExpression="c_DOCUMENTO">
                                        <ItemStyle Font-Bold="True" Font-Size="Large" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="c_DESCRIPCION" HeaderText="PROVEEDOR" SortExpression="c_DESCRIPCION">
                                        <ItemStyle Width="620px" Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="NO DESPACHADO">
                                        <ItemStyle Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EN ESPERA">
                                        <ItemStyle Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RECIBIENDOSE">
                                        <ItemStyle Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="RECIBIDO">
                                        <ItemStyle Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="enc_DespachoOdc_status" SortExpression="enc_DespachoOdc_status" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="m_LabelStatus" runat="server" Text='<%# Bind("enc_DespachoOdc_status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="enc_DespachoOdc_FueraDeFecha" SortExpression="enc_DespachoOdc_FueraDeFecha" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="m_LabelFueraDeFecha" runat="server" Text='<%# Bind("enc_DespachoOdc_FueraDeFecha") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSourceODCPorRecibirManon" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rp_ListadoDeOdcPorRecibir" SelectCommandType="StoredProcedure" OnSelecting="SqlDataSourceODCPorRecibirManon_Selecting" CancelSelectOnNullParameter="False">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="m_LabelFecha" DbType="Date" DefaultValue="" Name="FechaRecepcion" PropertyName="Text" />
                                    <asp:Parameter DefaultValue="UNI02" Name="sucursal" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        </div>
    </form>
</body>
</html>
