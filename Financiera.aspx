﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Financiera.aspx.cs" Inherits="Financiera" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cartelera</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />


    <script src="Scripts/jquery-1.10.2.min.js"></script>

    <script src="Scripts/bxslider/jquery.bxslider.min.js"></script>
    <link href="Scripts/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <style>
        .Fullscreen {
            width: 1880px !important;
            padding-left: 5px;
        }

        .panel-wrapper {
            padding-top: 5px;
            padding-left: 15px;
        }

        .HeaderFix {
            text-align: center;
            height: 45px;
            vertical-align: middle;
            margin: 0 0 5px 0 !important;
            padding: 3px 0 !important;
            font-weight: bold;
            border-radius: 15px;
        }

        .bx-prev {
            position: absolute !important;
            top: 50px !important;
        }

        .bx-next {
            position: absolute !important;
            top: 50px !important;
        }

        .bx-controls-auto {
            position: absolute !important;
            top: 10px !important;
            text-align: left !important;
            padding-left: 5px;
            width: 10% !important;
            height: 10px;
        }
    </style>
    <script>

        $(function () {
            $('.bxslider').bxSlider({
                auto: true,
                speed: 3000,
                slideMargin: 5,
                controls: true,
                pause: 30000,
                //autoHover: true,
                autoControls: true,
                pager: false
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <ul class="bxslider">
            <li>
                <div class="Fullscreen">
                    <h1 style="background-color: #0EA554" class="HeaderFix"><b>SUCURSAL PREBO III</b><asp:Label ID="m_labelFecha" runat="server" Visible="False"></asp:Label>
                    </h1>
                    <div class="row">
                        <div class="col-lg-12">
                            <asp:UpdatePanel runat="server" ID="_updateTimePrebo" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="Label1" runat="server" Visible="False"></asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="row">
                        <h1 class="HeaderFix">PRODUCTOS SIN ROTACION</h1>
                        <asp:UpdatePanel runat="server" ID="_UpdatePanelPrebo" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-lg-6">
                                    <h1 style="background-color: #0EA554" class="HeaderFix"><b>PERECEDEROS</b></h1>
                                    <asp:GridView ID="_GridViewSinRotacionPreboPerece" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSinRotacionPreboPerece" CssClass="table table-hover table-striped table-responsive table-bordered" EmptyDataText="No hay ordenes de compra para el dia de Hoy" DataKeyNames="c_codlocalidad,c_coddeposito">
                                        <Columns>
                                            <asp:BoundField DataField="c_CODARTICULO" HeaderText="Cod. Articulo" SortExpression="c_CODARTICULO" />
                                            <asp:BoundField DataField="C_DESCRI" HeaderText="Descripcion" SortExpression="C_DESCRI" />
                                            <asp:BoundField DataField="C_DESCRIPCIO" HeaderText="Dpto." SortExpression="C_DESCRIPCIO" />
                                            <asp:BoundField DataField="c_departamento" HeaderText="Dpto. codigo" SortExpression="c_departamento" Visible="False" />
                                            <asp:BoundField DataField="UltimaVenta" HeaderText="UltimaVenta" SortExpression="UltimaVenta" ReadOnly="True" DataFormatString="{0:d}"></asp:BoundField>
                                            <asp:BoundField DataField="UltimaRecepcion" HeaderText="UltimaRecepcion" ReadOnly="True" SortExpression="UltimaRecepcion" DataFormatString="{0:d}" />
                                            <asp:BoundField DataField="c_codlocalidad" HeaderText="c_codlocalidad" ReadOnly="True" SortExpression="c_codlocalidad" Visible="False" />
                                            <asp:BoundField DataField="c_coddeposito" HeaderText="c_coddeposito" ReadOnly="True" SortExpression="c_coddeposito" Visible="False" />
                                            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" SortExpression="Cantidad" DataFormatString="{0:n2}" />
                                            <asp:BoundField DataField="DiasSinRotacion" HeaderText="Dias" ReadOnly="True" SortExpression="DiasSinRotacion" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSourceSinRotacionPreboPerece" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rps_ProductosSinVenta" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="False">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="m_labelFecha" DbType="Date" DefaultValue="" Name="FechaDesde" PropertyName="Text" />
                                            <asp:Parameter DefaultValue="CAR, CHA, RYC, VYH, PES" Name="Departamentos" Type="String" />
                                            <asp:Parameter DefaultValue="UNI01" Name="CodLocalidad" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>

                                </div>
                                <div class="col-lg-6">
                                    <h1 style="background-color: #0EA554" class="HeaderFix"><b>NO PERECEDEROS</b></h1>
                                    <asp:GridView ID="_GridViewSinRotacionPreboNoPerece" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSinRotacionPreboNoPerece" CssClass="table table-hover table-striped table-responsive table-bordered" EmptyDataText="No hay ordenes de compra para el dia de Hoy" DataKeyNames="c_codlocalidad,c_coddeposito">
                                        <Columns>
                                            <asp:BoundField DataField="c_CODARTICULO" HeaderText="Cod. Articulo" SortExpression="c_CODARTICULO" />
                                            <asp:BoundField DataField="C_DESCRI" HeaderText="Descripcion" SortExpression="C_DESCRI" />
                                            <asp:BoundField DataField="C_DESCRIPCIO" HeaderText="Dpto." SortExpression="C_DESCRIPCIO" />
                                            <asp:BoundField DataField="c_departamento" HeaderText="Dpto. codigo" SortExpression="c_departamento" Visible="False" />
                                            <asp:BoundField DataField="UltimaVenta" HeaderText="UltimaVenta" SortExpression="UltimaVenta" ReadOnly="True" DataFormatString="{0:d}"></asp:BoundField>
                                            <asp:BoundField DataField="UltimaRecepcion" HeaderText="UltimaRecepcion" ReadOnly="True" SortExpression="UltimaRecepcion" DataFormatString="{0:d}" />
                                            <asp:BoundField DataField="c_codlocalidad" HeaderText="c_codlocalidad" ReadOnly="True" SortExpression="c_codlocalidad" Visible="False" />
                                            <asp:BoundField DataField="c_coddeposito" HeaderText="c_coddeposito" ReadOnly="True" SortExpression="c_coddeposito" Visible="False" />
                                            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" SortExpression="Cantidad" DataFormatString="{0:n2}" />
                                            <asp:BoundField DataField="DiasSinRotacion" HeaderText="Dias" ReadOnly="True" SortExpression="DiasSinRotacion" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSourceSinRotacionPreboNoPerece" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rps_ProductosSinVenta" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="False">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="m_labelFecha" DbType="Date" DefaultValue="" Name="FechaDesde" PropertyName="Text" />
                                            <asp:Parameter DefaultValue="ANI, AUT, CAM, CMT, ELE,  HOG, JAR, JUG, LIC, LIM, NAT, VYV" Name="Departamentos" Type="String" />
                                            <asp:Parameter DefaultValue="UNI01" Name="CodLocalidad" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </li>
            <li>
                <div class="Fullscreen">
                    <h1 style="background-color: #F87F30" class="HeaderFix"><b>SUCURSAL MAÑONGO</b></h1>
                    <div class="row">
                        <h1 class="HeaderFix">PRODUCTOS SIN ROTACION</h1>
                        <asp:UpdatePanel runat="server" ID="_UpdatePanelManon" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="col-lg-6">
                                    <h1 style="background-color: #F87F30" class="HeaderFix"><b>PERECEDEROS</b></h1>
                                    <asp:GridView ID="_GridViewSinRotacionManonPerece" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSinRotacionManonPerece" CssClass="table table-hover table-striped table-responsive table-bordered" EmptyDataText="No hay ordenes de compra para el dia de Hoy" DataKeyNames="c_codlocalidad,c_coddeposito">
                                        <Columns>
                                            <asp:BoundField DataField="c_CODARTICULO" HeaderText="Cod.  Articulo" SortExpression="c_CODARTICULO" />
                                            <asp:BoundField DataField="C_DESCRI" HeaderText="Descripcion" SortExpression="C_DESCRI" />
                                            <asp:BoundField DataField="C_DESCRIPCIO" HeaderText="Dpto." SortExpression="C_DESCRIPCIO" />
                                            <asp:BoundField DataField="c_departamento" HeaderText="Dpto. codigo" SortExpression="c_departamento" Visible="False" />
                                            <asp:BoundField DataField="UltimaVenta" HeaderText="UltimaVenta" ReadOnly="True" SortExpression="UltimaVenta" DataFormatString="{0:d}" />
                                            <asp:BoundField DataField="UltimaRecepcion" HeaderText="UltimaRecepcion" ReadOnly="True" SortExpression="UltimaRecepcion" DataFormatString="{0:d}" />
                                            <asp:BoundField DataField="c_codlocalidad" HeaderText="c_codlocalidad" ReadOnly="True" SortExpression="c_codlocalidad" Visible="False" />
                                            <asp:BoundField DataField="c_coddeposito" HeaderText="c_coddeposito" ReadOnly="True" SortExpression="c_coddeposito" Visible="False" />
                                            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" SortExpression="Cantidad" DataFormatString="{0:n2}" />
                                            <asp:BoundField DataField="DiasSinRotacion" HeaderText="Dias" ReadOnly="True" SortExpression="DiasSinRotacion" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSourceSinRotacionManonPerece" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rps_ProductosSinVenta" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="False">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="m_labelFecha" DbType="Date" DefaultValue="" Name="FechaDesde" PropertyName="Text" />
                                            <asp:Parameter DefaultValue="CAR, CHA, RYC, VYH, PES" Name="Departamentos" Type="String" />
                                            <asp:Parameter DefaultValue="UNI02" Name="CodLocalidad" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>

                                </div>
                                <div class="col-lg-6">
                                    <h1 style="background-color: #F87F30" class="HeaderFix"><b>NO PERECEDEROS</b></h1>
                                    <asp:GridView ID="_GridViewSinRotacionManonNoPerece" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSourceSinRotacionPreboNoPerece" CssClass="table table-hover table-striped table-responsive table-bordered" EmptyDataText="No hay ordenes de compra para el dia de Hoy" DataKeyNames="c_codlocalidad,c_coddeposito">
                                        <Columns>
                                            <asp:BoundField DataField="c_CODARTICULO" HeaderText="Cod.  Articulo" SortExpression="c_CODARTICULO" />
                                            <asp:BoundField DataField="C_DESCRI" HeaderText="Descripcion" SortExpression="C_DESCRI" />
                                            <asp:BoundField DataField="C_DESCRIPCIO" HeaderText="Dpto." SortExpression="C_DESCRIPCIO" />
                                            <asp:BoundField DataField="c_departamento" HeaderText="Dpto. codigo" SortExpression="c_departamento" Visible="False" />
                                            <asp:BoundField DataField="UltimaVenta" HeaderText="UltimaVenta" ReadOnly="True" SortExpression="UltimaVenta" DataFormatString="{0:d}" />
                                            <asp:BoundField DataField="UltimaRecepcion" HeaderText="UltimaRecepcion" ReadOnly="True" SortExpression="UltimaRecepcion" DataFormatString="{0:d}" />
                                            <asp:BoundField DataField="c_codlocalidad" HeaderText="c_codlocalidad" ReadOnly="True" SortExpression="c_codlocalidad" Visible="False" />
                                            <asp:BoundField DataField="c_coddeposito" HeaderText="c_coddeposito" ReadOnly="True" SortExpression="c_coddeposito" Visible="False" />
                                            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" SortExpression="Cantidad" DataFormatString="{0:n2}" />
                                            <asp:BoundField DataField="DiasSinRotacion" HeaderText="Dias" ReadOnly="True" SortExpression="DiasSinRotacion" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="SqlDataSourceSinRotacionManonNoPerece" runat="server" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rps_ProductosSinVenta" SelectCommandType="StoredProcedure" CancelSelectOnNullParameter="False">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="m_labelFecha" DbType="Date" DefaultValue="" Name="FechaDesde" PropertyName="Text" />
                                            <asp:Parameter DefaultValue="ANI, AUT, CAM, CMT, ELE,  HOG, JAR, JUG, LIC, LIM, NAT, VYV" Name="Departamentos" Type="String" />
                                            <asp:Parameter DefaultValue="UNI02" Name="CodLocalidad" Type="String" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </li>
                <%--           <div class="Fullscreen">
                <h1 style="background-color: #1E3B97;color:white" class="HeaderFix"><b>SUCURSAL SAN FELIPE</b></h1>
                <div class="row">
                    <div class="col-lg-8">
                        <asp:UpdatePanel runat="server" ID="_UpdatePanelSanFelipe" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Timer runat="server" ID="Timer2" Interval="10000" OnTick="Timer1_Tick"></asp:Timer>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>
           <div class="Fullscreen">
                <h1 style="background-color: #DF0889" class="HeaderFix"><b>SUCURSAL SAN DIEGO</b></h1>
                <div class="row">
                    <div class="col-lg-8">
                        <asp:UpdatePanel runat="server" ID="_UpdatePanelSanDiego" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Timer runat="server" ID="Timer3" Interval="10000" OnTick="Timer1_Tick"></asp:Timer>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="col-lg-2">
                    </div>
                </div>
            </div>--%>
            <li>
<%--                <div class="Fullscreen">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 style="background-color: #D9534F" class="HeaderFix">ODC POR VIAJAR</h1>
                            <asp:UpdatePanel runat="server" ID="_UpdatePanelPorViajar" UpdateMode="Conditional" ViewStateMode="Disabled">
                                <ContentTemplate>
                                    <div>
                                        <asp:Label ID="m_LabelSemaforo" runat="server" Text=""></asp:Label>
                                    </div>
                                    <asp:GridView ID="m_GridViewODCSinViajar" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-hover" EmptyDataText="Todas las Ordenes viajaron de manera Exitosa.">
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="col-md-6">
                            <h1 style="background-color: #D9534F" class="HeaderFix">ODC POR VENCER</h1>
                            <asp:GridView ID="_GridViewPorVencer" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-hover">
                                <Columns>
                                    <asp:TemplateField ConvertEmptyStringToNull="False" SortExpression="codusuario">
                                        <ItemTemplate>
                                            <asp:Label ID="m_LabelCodUsuario" runat="server" Text='<%# Eval("codusuario") %>' Visible="False"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="c_DOCUMENTO" HeaderText="# Orden" SortExpression="c_DOCUMENTO">
                                        <ItemStyle Font-Bold="True" Font-Size="Large" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="c_DESCRIPCION" HeaderText="PROVEEDOR" SortExpression="c_DESCRIPCION">
                                        <ItemStyle Font-Bold="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DU_FECHAVENCIMIENTO" HeaderText="VENCIMIENTO" SortExpression="DU_FECHAVENCIMIENTO" DataFormatString="{0:d}" />
                                    <asp:BoundField DataField="d_FECHA" HeaderText="d_FECHA" SortExpression="d_FECHA" Visible="False" />
                                    <asp:BoundField DataField="C_DESPACHAR" HeaderText="C_DESPACHAR" SortExpression="C_DESPACHAR" Visible="False" />
                                    <asp:BoundField DataField="c_CODLOCALIDAD" HeaderText="c_CODLOCALIDAD" SortExpression="c_CODLOCALIDAD" Visible="False" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="SqlDataSourcePorVencer" runat="server" CancelSelectOnNullParameter="False" ConnectionString="<%$ ConnectionStrings:DeveloperProceduresConnectionString %>" SelectCommand="rp_OdcPorVencer" SelectCommandType="StoredProcedure">
                                <SelectParameters>
                                    <asp:Parameter Name="Sucursal" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </div>
                    </div>
                </div>--%>
            </li>
            <li>
                <div class="Fullscreen">
                    <h1 class="HeaderFix">LEYENDA</h1>
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="HeaderFix" style="background-color: #0EA554;">SUCURSAL PREBO</h2>
                        </div>
                        <div class="col-md-6">
                            <h2 class="HeaderFix" style="background-color: #F87F30;">SUCURSAL MAÑONGO</h2>
                        </div>
                        <div class="col-md-6">
                            <h2 class="HeaderFix" style="background-color: #1E3B97; color: white;">SUCURSAL SAN FELIPE</h2>
                        </div>
                        <div class="col-md-6">
                            <h2 class="HeaderFix" style="background-color: #DF0889;">SUCURSAL SAN DIEGO</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="HeaderFix">&nbsp;</h2>
                        </div>
                        <div class="col-md-10 col-md-offset-1">
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DashboardsConnectionString %>" SelectCommand="SELECT [man_Sedes_Id], [man_Sedes_Nombre] FROM [man_Sedes]"></asp:SqlDataSource>
    </form>
</body>
</html>
